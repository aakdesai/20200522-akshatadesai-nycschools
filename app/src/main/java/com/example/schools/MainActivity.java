package com.example.schools;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.schools.adapters.SchoolAdapter;
import com.example.schools.model.SchoolDO;
import com.example.schools.service.SchoolApi;
import com.example.schools.service.ServiceGenerator;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Main Activity class
 */
public class MainActivity extends AppCompatActivity implements SchoolAdapter.OnSchoolItemListener {

    private List<SchoolDO> schools;
    private  RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Preparing recyclerview
        recyclerView = (RecyclerView) findViewById(R.id.schoolRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Create object to hold the base URL + tail
        SchoolApi apiService = ServiceGenerator.getClient().create(SchoolApi.class);

        // Call service using retrofit
        Call<List<SchoolDO>> call = apiService.getSchools();
        call.enqueue(new Callback<List<SchoolDO>>() {
            @Override
            public void onResponse(Call<List<SchoolDO>> call, Response<List<SchoolDO>> response) {
                schools = response.body();
                setAdapter();
            }
            @Override
            public void onFailure(Call<List<SchoolDO>> call, Throwable t) {
                Log.e("OnFailureError", t.toString());
            }
        });
    }

    /**
     * Executing on click event
     *
     * @param position
     */
    @Override
    public void onSchoolClick(int position) {
        Intent intent= new Intent(this, SchoolDetailActivity.class);
        intent.putExtra("schoolID", schools.get(position).getSchoolID());
        intent.putExtra("faxNumber", schools.get(position).getFaxNumber());
        intent.putExtra("phoneNumber", schools.get(position).getPhoneNumber());
        intent.putExtra("SchoolEmail", schools.get(position).getSchoolEmail());
        startActivity(intent);
    }

    /**
     * Set school adapter
     */
    public void setAdapter(){
        SchoolAdapter schoolAdapter = new SchoolAdapter(schools,this);
        recyclerView.setAdapter(schoolAdapter);
    }
}