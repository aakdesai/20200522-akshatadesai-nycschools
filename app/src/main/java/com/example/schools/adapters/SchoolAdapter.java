package com.example.schools.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.schools.R;
import com.example.schools.model.SchoolDO;

import java.util.List;

/**
 *  School adapter class for recyclerview
 */
public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.ViewHolder> {

    private List<SchoolDO> schools;
    private OnSchoolItemListener schoolListener;

    /**
     * Constructor
     *
     * @param schools
     * @param schoolListener
     */
    public SchoolAdapter(List<SchoolDO> schools, OnSchoolItemListener schoolListener) {
        this.schools = schools;
        this.schoolListener = schoolListener;
    }

    /**
     * OnCreate ViewHolder
     *
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.school_list, parent, false);
        return new ViewHolder(view, schoolListener);
    }

    /**
     * OnBind ViewHolder
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final SchoolDO schoolDO = schools.get(position);
        holder.schoolNameTextView.setText(schoolDO.getSchoolName());
        holder.websiteTextView.setText(schoolDO.getSchoolWebsite());
    }

    /**
     * Method to get school list size
     *
     * @return
     */
    @Override
    public int getItemCount() {
        return schools.size();
    }

    /**
     *  Interface for school item event listener
     */
    public interface OnSchoolItemListener {
        void onSchoolClick(int position);
    }

    /**
     *  ViewHolder for recyclerview
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout schoolCardLayout;
        TextView schoolNameTextView;
        TextView websiteTextView;
        OnSchoolItemListener onSchoolItemListener;

        public ViewHolder(View itemView, OnSchoolItemListener onSchoolItemListener) {
            super(itemView);
            schoolNameTextView = (TextView) itemView.findViewById(R.id.schoolNameTextView);
            websiteTextView = (TextView) itemView.findViewById(R.id.websiteTextView);
            schoolCardLayout = (LinearLayout) itemView.findViewById(R.id.schoolCardLayout);
            this.onSchoolItemListener = onSchoolItemListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onSchoolItemListener.onSchoolClick(getAdapterPosition());
        }
    }
}
   