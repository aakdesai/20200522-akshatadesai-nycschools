package com.example.schools;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.schools.model.SchoolDetailDO;
import com.example.schools.service.SchoolApi;
import com.example.schools.service.ServiceGenerator;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * SchoolDetailActivity class
 */
public class SchoolDetailActivity extends AppCompatActivity {

    public static final String FAX_SYMBOL = " (F)";
    public static final String PHONE_SYMBOL = " (P)";

    private Bundle bundle;
    private List<SchoolDetailDO> schoolsDetails;
    TextView schoolNameTextView, satStudentsTextView, avgReadingTextView,avgWritingTextView,
            avgMathTextView,schoolEmailTextView,phoneNumberTextView,faxNumberTextView;
    String schoolId,faxNumber,phoneNumber,schoolEmail;
    TableLayout satTable;
    LinearLayout schoolDetailsLayout;

    /**
     * onCreate method for school detail activity
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_detail);

        schoolNameTextView = findViewById(R.id.schoolNameTextView);
        schoolEmailTextView = findViewById(R.id.schoolEmailTextView);
        phoneNumberTextView = findViewById(R.id.phoneNumberTextView);
        faxNumberTextView = findViewById(R.id.faxNumberTextView);
        satStudentsTextView = findViewById(R.id.satStudentsTextView);
        avgReadingTextView = findViewById(R.id.avgReadingTextView);
        avgWritingTextView = findViewById(R.id.avgWritingTextView);
        avgMathTextView = findViewById(R.id.avgMathTextView);
        schoolDetailsLayout = findViewById(R.id.schoolDetailsLayout);
        satTable = findViewById(R.id.satTable);

        // Create bundle for extracting Intent data from Main Activity
        bundle = new Bundle();
        if (bundle != null){
            schoolId = getIntent().getStringExtra("schoolID");
            faxNumber = getIntent().getStringExtra("faxNumber");
            phoneNumber = getIntent().getStringExtra("phoneNumber");
            schoolEmail = getIntent().getStringExtra("SchoolEmail");
        }

        // Create object to hold the base URL + tail
        SchoolApi apiService = ServiceGenerator.getClient().create(SchoolApi.class);

        // Call service using retrofit
        Call<List<SchoolDetailDO>> call = apiService.getSchoolsDetails();
         call.enqueue(new Callback<List<SchoolDetailDO>>() {
            @Override
            public void onResponse(Call<List<SchoolDetailDO>> call, Response<List<SchoolDetailDO>> response) {
                schoolsDetails = response.body();
                Boolean studentDetailAvailable= false;

                for (SchoolDetailDO schoolDetailDO : schoolsDetails){
                    if (schoolDetailDO != null && schoolId.equals(schoolDetailDO.getSchoolID())){
                        setSchoolSatDetails(schoolDetailDO);
                        setSchoolDesc();
                        studentDetailAvailable = true;
                        break;
                    }
                }
                if(!studentDetailAvailable){
                    schoolNameTextView.setText(R.string.errorDisplay);
                    satTable.setVisibility(View.GONE);
                    schoolDetailsLayout.setVisibility(View.GONE);
                }
            }
            @Override
            public void onFailure(Call<List<SchoolDetailDO>> call, Throwable t) {

                Log.d("Error","SchoolDetail Error");

            }
        });

    }

    /**
     * Set school SAT details in TextView
     *
     * @param schoolDetailDO
     */
    public void setSchoolSatDetails(SchoolDetailDO schoolDetailDO){
            schoolNameTextView.setText(schoolDetailDO.getSchoolName());
            satStudentsTextView.setText(schoolDetailDO.getSatStudents());
            avgReadingTextView.setText(schoolDetailDO.getAvgReadingScore());
            avgWritingTextView.setText(schoolDetailDO.getAvgWritingScore());
            avgMathTextView.setText(schoolDetailDO.getAvgMathScore());
    }

    /**
     * Set school description details in TextView
     */
    public void setSchoolDesc(){
        if (schoolEmail == null) {
            schoolEmailTextView.setVisibility(View.GONE);
        } else {
            schoolEmailTextView.setText(schoolEmail);
        }
        if (phoneNumber == null) {
            phoneNumberTextView.setVisibility(View.GONE);
        } else {
            phoneNumberTextView.setText(phoneNumber + PHONE_SYMBOL);
        }
        if (faxNumber == null) {
            faxNumberTextView.setVisibility(View.GONE);
        } else {
            faxNumberTextView.setText(faxNumber + FAX_SYMBOL);

            // given more time could have displayed website in webview on click of the website link

        }
    }
}
