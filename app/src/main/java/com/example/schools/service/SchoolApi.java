package com.example.schools.service;

import com.example.schools.model.SchoolDO;
import com.example.schools.model.SchoolDetailDO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 *  API for NYC High School
 */
public interface SchoolApi {

    // Api to get list of NYC schools
    @GET("s3k6-pzi2.json")
    Call<List<SchoolDO>> getSchools();

    // Api to get SAT results of NYC schools
    @GET("f9bf-2cp4.json")
    Call<List<SchoolDetailDO>> getSchoolsDetails();
}
