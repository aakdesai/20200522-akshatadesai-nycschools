package com.example.schools.service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Service generator class for NYC schools
 */
public class ServiceGenerator {

    public static final String BASE_URL = "https://data.cityofnewyork.us/resource/";
    private static Retrofit retrofit = null;

    /**
     * Method to return retrofit client
     *
     * @return
     */
    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
